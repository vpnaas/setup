#/usr/bin/env bash

export PUBLIC_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)
echo $PUBLIC_IPV4
mkdir -p /opt/vpnaas

## DOCKER ##

if ! command -v docker &> /dev/null
then
    bash <(curl -fsSL https://get.docker.com)
fi

## OPENVPN ##
cd /opt/vpnaas
curl -fsSL https://gitlab.com/vpnaas/setup/-/raw/main/docker-compose.yml -o docker-compose.yml
docker compose run --rm openvpn ovpn_genconfig -u udp://$PUBLIC_IPV4
docker compose run --rm openvpn ovpn_initpki nopass
docker compose up -d
